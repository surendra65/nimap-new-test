import { Component, OnInit } from '@angular/core';
import { ServiService } from '../servi.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
  
  details:any;
  userpop:FormGroup;
  SelectedFile: File = null;
  imgURL: string | ArrayBuffer;
  uploadFilePath:String=null;
  isValid: boolean=false;
  
  constructor(public servi:ServiService,public fb:FormBuilder) { }

  ngOnInit(): void {

     this.servi.empdetails()
    .subscribe(data=>this.details=data)

    this.userpop=this.fb.group({
      
      id:["",Validators.required],
      fname:["",[Validators.maxLength(10),Validators.pattern("[a-z/A-Z]*")]],
      lname:["",[Validators.required,Validators.pattern("[a-z/A-Z]*")]],
      email:["",[Validators.required,Validators.email]],
      number:["", [Validators.required, Validators.pattern("[0-9]*"),
      Validators.minLength(10),Validators.maxLength(10)]],
      age:["",Validators.required],
      address:["",Validators.required],
      country:["",Validators.required],
      state:["",Validators.required],
      image:["",Validators.required]
    })
    // this.vv=this.servi.aaa;
  // console.log(this.servi.aaa.fname)
    // console.log(this.vv.fname.value);
    // console.log(this.vv);
    // this.vv.fname=this.sss;
    // console.log(this.vv.fname)
    // this.vv=this.userpop.controls.sss.value
    // console.log(this.vv.controls.id.value);
    // this.sss=this.controls.id.value;
    // this.vv.controls.fname.value=this.sss;
  }
  

  upload(event) {
    if(event.target.files[0].size < 310*325)
    {
      this.isValid=false;
    this.SelectedFile = <File>event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.SelectedFile);
    reader.onload = (_event) => {
      
     
        this.imgURL = reader.result;
        this.userpop.value["ProfileImg"] = this.imgURL;
     
      }
    }
      else{

        this.isValid=true;
        
      }
      
    }
 click(){
  this.servi.empdetails()
  .subscribe(data=>this.details=data)
 }
 
 update(){
     this.servi.updateuser(this.userpop.value).subscribe(
    res=>{
      return alert("edit sucessfull");
    }
  )
}
 

  

}
