import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { userinter } from './userinter';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ServiService {


  public url = "http://localhost:3000/path";
  constructor(private http: HttpClient) { }

  empdetails(): Observable<userinter[]> {
    return this.http.get<userinter[]>(this.url);
  }

  puser (a): Observable<userinter[]> {
  return this.http.post<userinter[]>(this.url, a);

  }
  updateuser(u) :Observable<userinter[]>{
    return this.http.put<userinter[]>(this.url + '/' + u.id,u);
  }
}
