import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServiService } from '../servi.service';
import { HttpClient } from '@angular/common/http';
import { userinter } from '../userinter';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  abc: userinter[];
  height: any;
  width: any;
  isValid: boolean=false;

  constructor(public fb:FormBuilder ,public servi:ServiService, private http:HttpClient) { }
  formData:FormGroup;
  SelectedFile: File = null;
  imgURL: string | ArrayBuffer;
  uploadFilePath:String=null;
 sss:boolean;


  
  ngOnInit(): void {
    this.formData=this.fb.group({
      
       id:["",Validators.required],
      fname:["",[Validators.maxLength(10),Validators.pattern("[a-z/A-Z]*")]],
      lname:["",[Validators.required,Validators.pattern("[a-z/A-Z]*")]],
      email:["",[Validators.required,Validators.email]],
      number:["", [Validators.required, Validators.pattern("[0-9]*"),
               Validators.minLength(10),Validators.maxLength(10)]],
      age:["",Validators.required],
      address:["",Validators.required],
      country:["",Validators.required],
      state:["",Validators.required],
      image:["",Validators.required]
    })
  }
  
  click(formData) {

    if(this.isValid==false){
    if (this.formData.valid) {
     console.log("form submit",this.formData.value);
    
      this.servi. puser (this.formData.value).subscribe(
       res=>{ return alert("successfull")
      }
       
      )
    }
  }
  }


  upload(event) {
    

    if(event.target.files[0].size < 310*325)
    {
      this.isValid=false;
    this.SelectedFile = <File>event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.SelectedFile);
    reader.onload = (_event) => {
      
     
        this.imgURL = reader.result;
        this.formData.value["ProfileImg"] = this.imgURL;
     
      }
    }
      else{

        this.isValid=true;
        
      }
      
    }
  
  



 
}
  


